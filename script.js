console.log(iqTest("2 4 7 8 10"));

function iqTest(numbers){
  var list = numbers.split(' ').map(Number).map(x => x%2);
  return 1 + (list.reduce((a,b) => a+b) == 1 ? list.indexOf(1) : list.indexOf(0));
}

function multiplicationTable(row,col){
	var matrix = [];
	for(let i = 0; i<row;i++){
		matrix[i] = [];
		for(let j=0;j<col;j++){
			matrix[i][j] = (j+1)*(i+1);
		}
  }
  return matrix;
}

function queueTime(customers, n) {
  var w = new Array(n).fill(0);
  for (let t of customers) {
    let idx = w.indexOf(Math.min(...w));
    w[idx] += t;
  }
  return Math.max(...w);
}

function queueTime_better(customers, n) {
	var array = Array.apply(null, new Array(n)).map(Number.prototype.valueOf,0);
	for(var i=0;i<customers.length;i++){
		array[array.indexOf(Math.min(...array))] += customers[i];
	}
  return array[array.indexOf(Math.max(...array))] ;
}

function queueTime_old(customers, n) {
	customers.sort();
	var array = Array.apply(null, new Array(n)).map(Number.prototype.valueOf,0);
	while(customers.length != 0){
		array[array.indexOf(Math.min(...array))] += customers.splice(0,1)[0];
	}
  return array[array.indexOf(Math.max(...array))] ;
}

function likes(names){
	if(names.length == 0){
		return "no one likes this";
	}else if (names.length == 1){
		return names[0] + " likes this";
	} else if (names.length == 2){
		return names[0] + " and " + names[1] + " likes this";
	} else if (names.length == 3){
		return names[0] + ", " + names[1] + " and " + names[2] + " like this";
	}
	
	return names[0] + ", " + names[1] + " and " + (names.length-2) + " others like this";
}

function race(v1,v2,g){
	var seconds = 3600*g/(v2-v1);
	var hours = Math.floor(seconds/3600);
	seconds = seconds - hours*3600;
	var minutes = Math.floor(seconds/60);
	seconds = Math.floor(seconds - 60*minutes);
	return [hours,minutes,seconds];
}