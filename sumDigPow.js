console.log(test(135))

function sumDigPow(a, b) {
	var list = [];
  for(i = a; i<=b; i++){
    if(test(i)){
		list.push(i);
	}
  }
  return list;
}

function test(num){
	var list = toList(num);
	var n = 0;
	for(i=0; i<list.length; i++){
		n += Math.pow(list[i], i+1);
	}
	return n == num;
}

function toList(number){
	var list = [];
	while(number > 0){
		list.unshift(number%10);
		number = Math.floor(number/10);
	}
	return list;
}